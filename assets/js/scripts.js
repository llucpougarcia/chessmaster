jQuery.fn.exists = function(){ return this.length > 0; }
//EJECUTAMOS LA FUNCION INICIADOR AL CARGARSE LOS ELEMENTOS DE LA PAGINA
document.addEventListener("DOMContentLoaded", function() { iniciador2(); }, false);
$(document).ready(function(){
//EVITAMOS QUE SE PUEDA SELECCIONAR NINGUN ELEMENTO
  $("*").disableSelection(); 

    $('#ajedrez').bind("contextmenu",function(e){
        return false;
    });
   




//anular arrastre letras

/*Código para evitar selección, recuerda que en jQuery # indica que es un id, . que es un clase, sin nada de # o . indicas que es un tag html y tambien puedes poner secuencias como en CSS, por ejemplo: #miId span .oculta*/;
$('#tabs-4').attr('unselectable', 'on');
$('#tabs-4').css('MozUserSelect', 'none');//mozilla y derivados
$('#tabs-4').css('KhtmlUserSelect', 'none');//el safari por ejemplo
});

function iniciador2() {
	//CREAMOS VARIABLES E INICIALIZAMOS, LUEGO LAS USAREMOS MAS ADELANTE
  turno =0;
  idPartida="";
  fichasDB=[];
  nuevaPartida=false;
  tablero= $("#tablero");
  tableroW=tablero.width();
  tableroH=tablero.height();
  var jugador;
  var colorJugador;
  contador=0;
  variable=$("#clickMe");
  variable2=$("#clickMe2");
  contadorCementerio=0;
  finJuego=false;
  //HACEMOS QUE EL CONTENEDOR DE LAS FICHAS MUERTAS PUEDAN MOVERSE
  $( "#tumbasBlancas" ).sortable({containment: '#tumbasBlancas'});
    $( "#tumbasBlancas" ).disableSelection();
     $( "#tumbasNegras" ).sortable({containment: '#tumbasNegras'});
    $( "#tumbasNegras" ).disableSelection();


    //CREAMOS EL TABLERO CON SU NOMBRE Y SUS CASILLAS
  crearTablero();
  //CREAMOS TODAS LAS FICHAS
  crearFichas();
  contadorFichas=$("#ajedrez .ui-draggable").length;
  fichas=$("#ajedrez .ui-draggable");

//DESABILITAMOS LAS FICHAS DEL COLOR  CONTRARIO AL TURNO
  deshabilitarFichas();


//TRANSFORMAMOS NUMEROS A LETRAS PARA LAS CASILLAS
  crearAlfabeto();





	//AGREGAMOS UN BINDER EL CUAL SI APRETAMOS ENTER EN EL CHAT SE ENVIARA EL MESSAGE
	$(window).keypress(function(e) {
	    if(e.keyCode==13&& $("#messageBox").is(":focus")){
	    	//AÑADIREMOS EL MESSAGE AL CUADRO DE LA CONVERSACION
	    addMensaje();
	    }
	});
  
}
//FUNCION QUE AÑADE A LA CONVERSACION LA HORA Y EL MENSAJE ESCRITO
function addMensaje(){
    var tiempo = new Date();
	var hora = tiempo.getHours();
	var minuto = tiempo.getMinutes();

    $("#chatMessages").append(
      "<div class='container darker'>"+
        "<p>"+$("#messageBox").val()+"</p>"+
          "<span class='time-left'>"+hora+":"+minuto+"</span>"+
      "</div>");
	var objDiv = document.getElementById("chatMessages");
	//DESLIZAMOS EL CONTENEDOR DE LOS MENSAJES AL FINAL
	$(objDiv).animate({scrollTop:objDiv.scrollHeight},800);
	var mensaje={
	  tipo:"mensaje",
	  mensaje:$("#messageBox").val(),
	  jugador:jugador,
	  tiempo:hora+":"+minuto,
	  idPartida:idPartida
	}
	//ENVIAMOS EL MENSAJE MEDIANTE WEBSOCKETS
	send(JSON.stringify(mensaje));
	//LIMPIAMOS EL CUADRO DE ESCRITURA DE MENSAJE
	clearMensaje();
  }
//TRAS RECIBIR EL MENSAJE CONTRARIO LO MOSTRAMOS EN LA CONVERSACION
function addMensajeContrario(jS){

  if(jS.jugador!=jugador&&idPartida==jS.idPartida){
    $("#chatMessages").append(
  "<div class='container'>"+
    "<p>"+jS.mensaje+"</p>"+
      "<span class='time-left'>"+jS.tiempo+"</span>"+
  "</div>");
var objDiv = document.getElementById("chatMessages");
//DESLIZAMOS LA CONVERSACION
$(objDiv).animate({scrollTop:objDiv.scrollHeight},800);
  }


}
//LIMPIAMOS EL CUADRO DE ESCRITURA DE MENSAJE
function clearMensaje(){
$("#messageBox").val("");

}
//ELEGIMOS SI QUEREMOS UNA PARTDA NUEVA, CARGAR UNA YA EN CURSO O ANTIGUA
function elegirPartida(){

$( "#tabs-4" ).append( "<div id='elegirPartida' title='Partida'></div>" );

//ABRIMOS UN DIALOGO PARA ELEGIR
$( "#elegirPartida" ).dialog({
dialogClass: "no-close",
buttons: [
  {
    text: "NUEVA",
    click: function() {
        nuevaPartida=true;
        //ABRIRA UN DIALOGO CON EL NUMERO CREADO
        mostrarNumero();
      $( this ).dialog( "close" );
    }},{text: "CARGAR",
    click: function() {
    	//ABRIRA UN DIALOGO PARA INTRODUCIR EL NUMERO DE LA PARTIDA A CARGAR
     introducirNumero();
      $( this ).dialog( "close" );
    } 
  } 
]
});
}
//ELEGIMOS LA PARTIDA DE LA PANTALLA PARTIDAS ACTIVAS
function pickPartida(element){
	//ESCONDE PARTIDA ACTIVA

  $(".contenedorPartida").css("display","none");
  $("#tabs-4").css("display","block");
  color=$(element).attr("color");
    if(color=="blanco"){
      jugador=0;
    }else{
      jugador=1;
    }
    idPartida=$(element).attr("partidaid"); 
    //RECOLOCA TODAS LAS FICHAS
    colocarFichas();
    //RECIBE EL TURNO ACTUAL
    turnoDB=cogerTurno();
    turno=parseInt(turnoDB[0].Turno);
    cogerTurno();
    //COMPRUEBA EL TURNO ACTUAL
    checkearTurno();
    //MUESTRA EL NUMERO DE PARTIDA ACTIVA PARA TENERNOS INFORMADOS
    $("#idPartidaInfo span").html(idPartida);



}

  

//COMPRUEBA SI EL CONTENEDOR DE PARTIDA ESTA ACTIVO PARA ESCONDERLO
function checkContenedorPartida(){

  if($("#contenedorPartidas").exists()&& $("#tabs-4").css("display")=="none"){
	  $(".contenedorPartida").css("display","none");
	  $("#tabs-4").css("visibility","hidden");
	  $("#tabs-4").css("display","block");
  
  }

}
//ABRE EL DIALOGO PARA INTRODUCIR EL NUMERO A CARGAR DE PARTIDA Y PODER CARGARLA
function introducirNumero(){
	deshabilitarFichas();
	checkContenedorPartida();
	nuevaPartida=false;
	$( "#tabs-4" ).append( "<div id='introducirNumero' title='NumeroPartida'><span>GameID</span> <input type='text' pattern='[0-9]{5}' maxlength= '5' id='idGameBox'><br></div>" );
	$( "#introducirNumero" ).dialog({
    dialogClass: "",
    buttons: [
      {
        text: "CARGAR",
        click: function() {
          idPartida=$("#idGameBox").val();
          $("#idPartidaInfo span").html(idPartida);
          $( this ).dialog( "close" ).remove();
          //ABRE UN DIALOGO PARA ELEGIR COLOR
          elegirColor();
        }}
  	]
	});
}
//MUESTRA EL NUMERO DE PARTIDA TRAS CREAR UNA PARTIDA NUEVA CREANDO 4 NUMEROS ALEATORIOS Y JUNTANDOLOS CREANDO UN NUMERO DE 4 DIGITOS
function mostrarNumero(){
	checkContenedorPartida();
	deshabilitarFichas();
	nuevaPartida=true;
	var numeroAleatorio="";
	for(i=0;i<5;i++){
	numeroAleatorio+=Math.floor((Math.random() * 9)+1);
	}
	idPartida=numeroAleatorio;
	$("#idPartidaInfo span").html(idPartida);
	// crearPartida();
	$( "#tabs-4" ).append( "<div id='mostrarNumero' title='NumeroPartida'><p class='randomNumber'>"+numeroAleatorio+"</p></div>" );


	$( "#mostrarNumero" ).dialog({
	dialogClass: "no-close",
	buttons: [
	  {
	    text: "VISTO",
	    click: function() {
	      
	      $( this ).dialog( "close" ).remove();
	      //ABRE UN DIALOGO PARA ELEGIR COLOR
	      elegirColor();
	    }}
	]
	});
}
//COMPRUEBA EL COLOR ACTUAL DE LA PARTIDA
function checkColor(){
var returnColor;
 $.ajax({
    url: '/fichas/checkColor',
    dataType: "json",
    async:false,
    data: {idPartida:idPartida},
    type: 'POST',
    
    success: function(data) {
    returnColor=data[0].Color;

     },
     error: function (xhr, ajaxOptions, thrownError) {
  }
});

 return returnColor;
}
//COGE EL INVITADO DE LA PARTIDA Y LO CAMBIA
function takeGuest(){

$.ajax({
    url: '/fichas/takeGuest',
    dataType: "json",
    async:false,
    data: {idPartida:idPartida},
    type: 'POST',
    
    success: function(data) {

 

     },
     error: function (xhr, ajaxOptions, thrownError) {

  }
});

}
//DESABILITA EL BOTON DEL COLOR QUE YA ESTA SELECCIONANDO EVITANDO DUPLICACION DE COLOR
function disableButton(c){

	var disableColorButon="";
	if(c=="blanco"){
	  disableColorButon="BLANCAS";
	}
	if(c=="negro"){
	  disableColorButon="NEGRAS";

	}
	$(".ui-dialog-buttonpane button:contains('"+disableColorButon+"')").button("disable");

	$(".ui-dialog-buttonpane button:contains('"+disableColorButon+"')").attr("disabled", true).addClass("ui-state-disabled");

}
//MUESTRA DIALOGO PARA ELEGIR COLOR 
function elegirColor(){

	$( "#tabs-4" ).append( "<div id='elegirColor' title='Elige'></div>" );


	$( "#elegirColor" ).dialog({
	dialogClass: "no-close",
	buttons: [
	  {
	    text: "BLANCAS",
	    click: function() {
	    	//CARGA UN LOADER A LA RESPUESTA DE LA PETICIONA  BDD
	    	//CREA PARTIDA, COLOCA FICHAS, CAMBIA TURNO Y CIERRA DIALOGO
	      var tis=this;
	      $("body").append('<div class="wrapperLoader"><button class="loader btn btn-lg btn-warning"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...</button></div>');
	      setTimeout(function(){
          $(".wrapperLoader").remove();
          jugador=0;
	      if(nuevaPartida==true){crearPartida();}
	      if(nuevaPartida==false){takeGuest();}
	      colocarFichas();
	      turnoDB=cogerTurno();
	      turno=parseInt(turnoDB[0].Turno);
	      checkearTurno();
	      $( tis).dialog( "close" ).remove();

	      },2000);
	     
	    }},{text: "NEGRAS",
	    click: function() {
	    	//CARGA UN LOADER A LA RESPUESTA DE LA PETICIONA  BDD
	    	//CREA PARTIDA, COLOCA FICHAS, CAMBIA TURNO Y CIERRA DIALOGO
	    	var tis=this;
	      $("body").append('<div class="wrapperLoader"><button class="loader btn btn-lg btn-warning"><span class="glyphicon glyphicon-refresh glyphicon-refresh-animate"></span> Cargando...</button></div>');
	      setTimeout(function(){
	      $(".wrapperLoader").remove();
	      jugador=1;
	      if(nuevaPartida==true){crearPartida();}
	      if(nuevaPartida==false){takeGuest();}
	      colocarFichas();
	      turnoDB=cogerTurno();
	      turno=parseInt(turnoDB[0].Turno);
	      cogerTurno();
	      checkearTurno();
	      $( tis).dialog( "close" ).remove();
	      },2000);
	    } 
	  } 
	]
	});

	if(nuevaPartida==false){
		//COMPRUEBA EL COLOR DISPONIBLE Y DESABILITA EL OTRO
	colorDisponible=checkColor();
	disableButton(colorDisponible);
	}


}
//COMPRUEBA QUE ALS CREDENCIALES DE LOGIN SEAN CORRECTAS
function checkUserPwd(){
  console.log("CHECK");
  var usuario=$("#UserName").val();
  var password=$("#PwdUser").val();
  $.ajax({
        url: '/login/checkUserPsswd',
        dataType: "json",
        async:false,
        data: {user:usuario,pwd:password},
        type: 'POST',
        
        success: function(data) {
        if(data==true){

          alert("Logeado");
        }else{

          alert("ERRORCREDENCIALES");
        }

         },
         error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });
}
//CAMBIA EL TURNO Y LO PONE DE NUESTRO COLOR
function cogerTurno(){
var returnTurn;
  $.ajax({
        url: '/fichas/cogerTurno',
        dataType: "json",
        async:false,
        data: {idPartida:idPartida},
        type: 'POST',
        
        success: function(data) {
        returnTurn=data;

         },
         error: function (xhr, ajaxOptions, thrownError) {
      }
    });
  return returnTurn;
}
//CREA UNA PARTIDA CON NUESTRO COLOR Y NUESTRO ID DE JUGADOR
function crearPartida(){
  console.log(jugador);
  if(jugador==0){
    colorJugador="blanco";
  }else{
    colorJugador="negro";
  }
$.ajax({
        url: '/fichas/crearPartida',
        dataType: "json",
        async:false,
        data: {idPartida:idPartida,Color:colorJugador},
        type: 'POST',
        
        success: function(data) {
        

         },
         error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });

}
//COLOCA LAS FICHAS EN EL TABLERO 
function colocarFichas(){
// SIE S NUEVA PARTIDA LAS COLOCA DE 0
  if(nuevaPartida==true){
    crearDBFichas();
    fichasDB=pedirFichas();
    reordenarFichas();
  }
  //SI LA PARTIDA ES CARGADA CARGGA LAS POSICIONES DE LA BBDD
  if(nuevaPartida==false){
    fichasDB=pedirFichas();
    reordenarFichas();
  }

}
//COLOCA LAS FICHAS DONDE SE QUEDARON EN ANTERIORES PARTIDAS, MOVIEMIENTOS, ETC
function reordenarFichas(){

limpiarCasillas();
//HACE UN BUCLE CON TODAS LAS FICHAS Y CASILLAS Y LAS COLOCA
  for(i=0;i<fichasDB.length;i++){
    var primerDB;
    var idFichaDB="#"+fichasDB[i].Nombre;
    var casillaDB="#casilla"+fichasDB[i].EjeX+"-"+fichasDB[i].EjeY;
    if(parseInt(fichasDB[i].Primer)==0){

      primerDB=false;
    }else{
      primerDB=true;

    }

    $(idFichaDB).animate("slow").position({my: "center",at: "center",of: casillaDB});
    $(idFichaDB).data("posicionX",parseInt(fichasDB[i].EjeX));
    $(idFichaDB).data("posicionY",parseInt(fichasDB[i].EjeY));
    $(idFichaDB).data("posicionY",parseInt(fichasDB[i].EjeY));
    $(idFichaDB).data("primer",primerDB);

    $(casillaDB).data("color",fichasDB[i].Color);
    //SI LA FICHA ESTA MUERTA NO LA COLOCA
    if(fichasDB[i].Muerta==1){
      $(idFichaDB).css('visibility','hidden');
    }else{

      $(casillaDB).data("ficha",fichasDB[i].Nombre);
      $(casillaDB).data("ocupado",true);
    }

    
  }
}
//CAMBIA LA POSICION DE UNA FICHA DE UNA CASILLA A OTRA
function cambiarPosicionFicha(ficha,posicionX,posicionY){
       $.ajax({
        url: '/fichas/cambiarPosicion',
        dataType: "json",
        async:true,
        data: {idPartida:idPartida,nombreFicha:ficha,ejeX:posicionX,ejeY:posicionY},
        type: 'POST',
        
        success: function(data) {
        returnFichas=data;

         },
         error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });


}
//LIMPIA LOS DATOS ALMACENADOS DE LAS FICHAS EN SU CASILLA ANTIGUA 
function limpiarCasillas(){
var casillasTablero=$(".casillatablero");
  for(i=0;i<casillasTablero.length;i++){

    $(casillasTablero[i]).data("ocupado",false);
    $(casillasTablero[i]).data("color","none");
    $(casillasTablero[i]).data("ficha","none");

  }
}
//PIDE TODAS LAS FICHAS A LA BDD
function pedirFichas(){
  var returnFichas;
     $.ajax({
        url: '/fichas/cogerFichas',
        dataType: "json",
        async:false,
        data: {idPartida:idPartida},
        type: 'POST',
        
        success: function(data) {
        returnFichas=data;

         },
         error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });

     return returnFichas;
}
//CREA TODAS LAS FICHAS EN LA BDD PARA SU POSTERIOR USO
function crearDBFichas(){
     
    $.ajax({
        url: '/fichas/crearFichas',
        async:false,
        dataType: "json",
        data: {idPartida:idPartida},
        type: 'POST',
        
        success: function(data) {


         },
         error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });

}
//DESHABILITA EL MOVIEMIENTO DE CIERTAS FICHAS, REVOCANDO SU MOVIMIENTO EN TURNO INCORRECTO O JUGADOR INAPROPIADO
function deshabilitarFichas(){
   $(".ui-draggable").draggable("disable");
}
//COMPRUEBA SI ES NUESTRO TURNO PAR APODER MOVER FICHAS
function checkearTurno(){
  
 if($("#tabs-4").css("visibility")=="hidden"){

  
  $("#tabs-4").css("visibility","visible");
 }

 contadorFichas=$("#ajedrez .ui-draggable").length;
 fichas=$("#ajedrez .ui-draggable");
 contadorCasillas=$("#tablero .ui-droppable").length;
 casillas=$("#tablero .ui-droppable");


  if(turno!=jugador){
    if(turno==1){
     disableColor="blanco";
    }
    if(turno==0){
      disableColor="negro";
    }

    

    for(i=0;i<contadorFichas;i++){


      if($(fichas[i]).data("color")==disableColor){

       $($("#ajedrez .ui-draggable")[i]).draggable("disable");
      }
    //  if(fichas.data("color")==enableColor){
      //  $($("#ajedrez .ui-draggable")[i]).draggable("enable");
      //}
    }
  }
  if(turno==jugador){

    if(turno==1){
      enableColor="negro";
    }
    if(turno==0){
      enableColor="blanco";
    }
    for(i=0;i<contadorFichas;i++){

      if($(fichas[i]).data("color")==enableColor){

       $($("#ajedrez .ui-draggable")[i]).draggable("enable");
      }

    }
    
  }

}
//FUNCION PRUEBA DE CERRAR MANO
  function cerrarMano(){
   



  }
//FUNCION DE MENSAJE DE FIN DE PARTIDA
function finPartida(color){
  if (color=="blanco"){cambioColor="negro"};
  if (color=="negro"){cambioColor="blanco"};
  $( "#tabs-4" ).append( "<div id='finito' title='Victoria'></div>" );
  $( "#finito" ).append( "<p>Victoria del jugador "+cambioColor+"</p>" );
   
    $( "#finito" ).dialog({height: 120,width:300});
      //setTimeout("$( '#finito' ).dialog('close')",50000);
     $( "#finito" ).dialog({
     close: function( event, ui ) { location.reload();}
     });

  
finJuego=true;




}
//ELIMINA LAS FICAHS MUERTAS Y CREA UNA COPIA EN EL CEMENTERIO PARA VISUALIZAR ALS FICAHS MUERTAS O CONSEGUIDAS POR EL CONTRARIO
function alCementerio(tipoFicha,color,nombreFicha){
//SWITCHCASE DEPENDIENDO EL TIPO DE FICHA QUE MATEMOS
  switch (tipoFicha) {
    case "peon":
    if (color=="blanco"){
 $( "#tumbasBlancas" ).append( "<li id='tumbaB"+contadorCementerio+"'></li>" );
  $("#tumbaB"+contadorCementerio).css({"background":"url(pawn.png)","background-size":"100%","float":"left"})
    }
    if (color=="negro"){
       $( "#tumbasNegras" ).append( "<li id='tumbaN"+contadorCementerio+"'></li>" );
  $("#tumbaN"+contadorCementerio).css({"background":"url(../images/pawnB.png)","background-size":"100%","float":"left"})
    }
 
  break;
  case "torre":
  if (color=="blanco"){
 $( "#tumbasBlancas" ).append( "<li id='tumbaB"+contadorCementerio+"'></li>" );
  $("#tumbaB"+contadorCementerio).css({"background":"url(rookW.png)","background-size":"100%","float":"left"})
    }
    if (color=="negro"){
       $( "#tumbasNegras" ).append( "<li id='tumbaN"+contadorCementerio+"'></li>" );
  $("#tumbaN"+contadorCementerio).css({"background":"url(rookB.png)","background-size":"100%","float":"left"})
    }

  break;
  case "caballo":
  if (color=="blanco"){
 $( "#tumbasBlancas" ).append( "<li id='tumbaB"+contadorCementerio+"'></li>" );
  $("#tumbaB"+contadorCementerio).css({"background":"url(/assets/css/images/horseW.png)no-repeat","background-size":"34px 45px","float":"left"})
    }
    if (color=="negro"){
       $( "#tumbasNegras" ).append( "<li id='tumbaN"+contadorCementerio+"'></li>" );
  $("#tumbaN"+contadorCementerio).css({"background":"url(/assets/css/images/horseB.png)no-repeat","background-size":"34px 45px","float":"left"})
    }
  break;
  case "alfil":
  if (color=="blanco"){
 $( "#tumbasBlancas" ).append( "<li id='tumbaB"+contadorCementerio+"'></li>" );
  $("#tumbaB"+contadorCementerio).css({"background":"url(bishopW.png)","background-size":"100%","float":"left"})
    }
    if (color=="negro"){
       $( "#tumbasNegras" ).append( "<li id='tumbaN"+contadorCementerio+"'></li>" );
  $("#tumbaN"+contadorCementerio).css({"background":"url(/assets/css/images/bishopB.png)","background-size":"100%","float":"left"})
    }
  break;
  case "rey":
  if (color=="blanco"){
 $( "#tumbasBlancas" ).append( "<li id='tumbaB"+contadorCementerio+"'></li>" );
  $("#tumbaB"+contadorCementerio).css({"background":"url(kingW.png)","background-size":"100%","float":"left"})
  //SI MATAMOS REY TERMINA LA PARTIDA
  finPartida(color);
    }
    if (color=="negro"){
       $( "#tumbasNegras" ).append( "<li id='tumbaN"+contadorCementerio+"'></li>" );
  $("#tumbaN"+contadorCementerio).css({"background":"url(kingB.png)","background-size":"100%","float":"left"})
  //SI MATAMOS REY TERMINA LA PARTIDA
  finPartida(color);
    }
  break;
  case "reina":
  if (color=="blanco"){
 $( "#tumbasBlancas" ).append( "<li id='tumbaB"+contadorCementerio+"'></li>" );
  $("#tumbaB"+contadorCementerio).css({"background":"url(queenW.png)","background-size":"100%","float":"left"})
    }
    if (color=="negro"){
       $( "#tumbasNegras" ).append( "<li id='tumbaN"+contadorCementerio+"'></li>" );
  $("#tumbaN"+contadorCementerio).css({"background":"url(queenB.png)","background-size":"100%","float":"left"})
    }
  break;
}
contadorCementerio++;
}
//CAMBIA DE NUMEROS A LETRAS
function crearAlfabeto(){
	my_string="a";

	my_string.substring(0,my_string.length-1)+String.fromCharCode(my_string.charCodeAt(my_string.length-1)+1);


}
//MARCAMOS EL POSIBLE MOVIMIENTO DE LA FICHA EN ROJO PARA FACILITAR A JUGADORES INEXPERTOS
  function casillasRojas( evs, ui ) {
    ficha=$(this).data( 'ficha' );
    primer=$(this).data( 'primer' );
    color=$(this).data( 'color' );
    fichaX = $(this).data( 'posicionX' );
    fichaY = $(this).data( 'posicionY' );
    ficha=$(this).data( 'ficha' );
    color=$(this).data( 'color' );
    nombreFicha=$(this).attr('id');
    casillaOcupada=false;
    posibleAlfil=false;
    posibleTorre=false;
    torreBorrado=false;
    cambioAlfil=false;



//COMPROBAMOS CON  UN SWITCHCASE QUE FICHA TENEMOS EN LA MANO
    switch (ficha) {
    case "peon":
    if (color=="negro"){
     


                      

  if($("#casilla"+(fichaX+1)+"-"+(fichaY+1)).data("ocupado")==true||$("#casilla"+(fichaX+1)+"-"+(fichaY-1)).data("ocupado")==true){
    if($("#casilla"+(fichaX+1)+"-"+(fichaY+1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX+1)+"-"+(fichaY+1)).data("color")){
      $("#casilla"+(fichaX+1)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX+1)+"-"+(fichaY-1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX+1)+"-"+(fichaY-1)).data("color"))){
      $("#casilla"+(fichaX+1)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }
      
    
  }
  //COMPROBAMOS SI ES SU PRIMER MOVIEMINETO
  if(primer==false){
    if($("#casilla"+(fichaX+2)+"-"+fichaY).data("ocupado")!=true){
      $("#casilla"+(fichaX+2)+"-"+fichaY).css({"opacity":"0.3"});
    }
    
  }
if($("#casilla"+(fichaX+1)+"-"+fichaY).data("ocupado")!=true){
    $("#casilla"+(fichaX+1)+"-"+fichaY).css({"opacity":"0.3"});
  }
  }

//COMPROBAMOS SU COLOR
    if (color=="blanco"){
    if($("#casilla"+(fichaX-1)+"-"+(fichaY+1)).data("ocupado")==true||$("#casilla"+(fichaX-1)+"-"+(fichaY-1)).data("ocupado")==true){
    if($("#casilla"+(fichaX-1)+"-"+(fichaY+1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX-1)+"-"+(fichaY+1)).data("color")){
      $("#casilla"+(fichaX-1)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX-1)+"-"+(fichaY-1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX-1)+"-"+(fichaY-1)).data("color")){
      $("#casilla"+(fichaX-1)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }
      
    
  }
  if(primer==false){
    if($("#casilla"+(fichaX-2)+"-"+fichaY).data("ocupado")!=true){
      $("#casilla"+(fichaX-2)+"-"+fichaY).css({"opacity":"0.3"});
    }
    
  }
if($("#casilla"+(fichaX-1)+"-"+fichaY).data("ocupado")!=true){
    $("#casilla"+(fichaX-1)+"-"+fichaY).css({"opacity":"0.3"});
  }
  }

    break;
    case "caballo":
    if($("#casilla"+(fichaX+2)+"-"+(fichaY+1)).data("ocupado")!=true){
    $("#casilla"+(fichaX+2)+"-"+(fichaY+1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX+2)+"-"+(fichaY-1)).data("ocupado")!=true){
    $("#casilla"+(fichaX+2)+"-"+(fichaY-1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX+1)+"-"+(fichaY+2)).data("ocupado")!=true){
    $("#casilla"+(fichaX+1)+"-"+(fichaY+2)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX+1)+"-"+(fichaY-2)).data("ocupado")!=true){
    $("#casilla"+(fichaX+1)+"-"+(fichaY-2)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-2)+"-"+(fichaY+1)).data("ocupado")!=true){
    $("#casilla"+(fichaX-2)+"-"+(fichaY+1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-2)+"-"+(fichaY-1)).data("ocupado")!=true){
    $("#casilla"+(fichaX-2)+"-"+(fichaY-1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-1)+"-"+(fichaY+2)).data("ocupado")!=true){
    $("#casilla"+(fichaX-1)+"-"+(fichaY+2)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-1)+"-"+(fichaY-2)).data("ocupado")!=true){
    $("#casilla"+(fichaX-1)+"-"+(fichaY-2)).css({"opacity":"0.3"});
  }

 //Ocupadas con enemigas
   if($("#casilla"+(fichaX+2)+"-"+(fichaY+1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX+2)+"-"+(fichaY+1)).data("color")){
    $("#casilla"+(fichaX+2)+"-"+(fichaY+1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX+2)+"-"+(fichaY-1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX+2)+"-"+(fichaY-1)).data("color")){
    $("#casilla"+(fichaX+2)+"-"+(fichaY-1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX+1)+"-"+(fichaY+2)).data("ocupado")==true&&color!=$("#casilla"+(fichaX+1)+"-"+(fichaY+2)).data("color")){
    $("#casilla"+(fichaX+1)+"-"+(fichaY+2)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX+1)+"-"+(fichaY-2)).data("ocupado")==true&&color!=$("#casilla"+(fichaX+1)+"-"+(fichaY-2)).data("color")){
    $("#casilla"+(fichaX+1)+"-"+(fichaY-2)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-2)+"-"+(fichaY+1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX-2)+"-"+(fichaY+1)).data("color")){
    $("#casilla"+(fichaX-2)+"-"+(fichaY+1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-2)+"-"+(fichaY-1)).data("ocupado")==true&&color!=$("#casilla"+(fichaX-2)+"-"+(fichaY-1)).data("color")){
    $("#casilla"+(fichaX-2)+"-"+(fichaY-1)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-1)+"-"+(fichaY+2)).data("ocupado")==true&&color!=$("#casilla"+(fichaX-1)+"-"+(fichaY+2)).data("color")){
    $("#casilla"+(fichaX-1)+"-"+(fichaY+2)).css({"opacity":"0.3"});
  }
     if($("#casilla"+(fichaX-1)+"-"+(fichaY-2)).data("ocupado")==true&&color!=$("#casilla"+(fichaX-1)+"-"+(fichaY-2)).data("color")){
    $("#casilla"+(fichaX-1)+"-"+(fichaY-2)).css({"opacity":"0.3"});
  }

    break;
    case"torre":
        for(i=(fichaX-1);i>=0;i--){
      if($("#casilla"+(i)+"-"+(fichaY)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(fichaY)).data("color")!=color){
          $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
    for(i=(fichaX+1);i<=7;i++){
      if($("#casilla"+(i)+"-"+(fichaY)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(fichaY)).data("color")!=color){
          $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=(fichaY+1);i<=7;i++){
      if($("#casilla"+(fichaX)+"-"+(i)).data("ocupado")!=true){
        $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(fichaX)+"-"+(i)).data("color")!=color){
          $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=(fichaY-1);i>=0;i--){
      if($("#casilla"+(fichaX)+"-"+(i)).data("ocupado")!=true){
        $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(fichaX)+"-"+(i)).data("color")!=color){
          $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
    break;
    case"alfil":
    for(i=fichaX-1,j=fichaY+1;i>=0&&j<=7;i--,j++){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
    for(i=fichaX+1,j=fichaY-1;i<=7&&j>=0;i++,j--){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=fichaX+1,j=fichaY+1;i<=7&&j<=7;i++,j++){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=fichaX-1,j=fichaY-1;i>=0&&j>=0;i--,j--){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }

    break;
    case"rey":
    if($("#casilla"+(fichaX-1)+"-"+(fichaY)).data("ocupado")!=true){
      $("#casilla"+(fichaX-1)+"-"+(fichaY)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX+1)+"-"+(fichaY)).data("ocupado")!=true){
      $("#casilla"+(fichaX+1)+"-"+(fichaY)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX-1)+"-"+(fichaY+1)).data("ocupado")!=true){
      $("#casilla"+(fichaX-1)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX-1)+"-"+(fichaY-1)).data("ocupado")!=true){
      $("#casilla"+(fichaX-1)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX)+"-"+(fichaY-1)).data("ocupado")!=true){
      $("#casilla"+(fichaX)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX)+"-"+(fichaY+1)).data("ocupado")!=true){
      $("#casilla"+(fichaX)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX+1)+"-"+(fichaY-1)).data("ocupado")!=true){
      $("#casilla"+(fichaX+1)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX+1)+"-"+(fichaY+1)).data("ocupado")!=true){
      $("#casilla"+(fichaX+1)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }

    //Ocupadas con enemigas
    if($("#casilla"+(fichaX-1)+"-"+(fichaY)).data("ocupado")==true&&color!=($("#casilla"+(fichaX-1)+"-"+(fichaY)).data("color"))){
      $("#casilla"+(fichaX-1)+"-"+(fichaY)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX+1)+"-"+(fichaY)).data("ocupado")==true&&color!=($("#casilla"+(fichaX+1)+"-"+(fichaY)).data("color"))){
      $("#casilla"+(fichaX+1)+"-"+(fichaY)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX-1)+"-"+(fichaY+1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX-1)+"-"+(fichaY+1)).data("color"))){
      $("#casilla"+(fichaX-1)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }
    if($("#casilla"+(fichaX-1)+"-"+(fichaY-1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX-1)+"-"+(fichaY-1)).data("color"))){
      $("#casilla"+(fichaX-1)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX)+"-"+(fichaY-1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX)+"-"+(fichaY-1)).data("color"))){
      $("#casilla"+(fichaX)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX)+"-"+(fichaY+1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX)+"-"+(fichaY+1)).data("color"))){
      $("#casilla"+(fichaX)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX+1)+"-"+(fichaY-1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX+1)+"-"+(fichaY-1)).data("color"))){
      $("#casilla"+(fichaX+1)+"-"+(fichaY-1)).css({"opacity":"0.3"});
    }if($("#casilla"+(fichaX+1)+"-"+(fichaY+1)).data("ocupado")==true&&color!=($("#casilla"+(fichaX+1)+"-"+(fichaY+1)).data("color"))){
      $("#casilla"+(fichaX+1)+"-"+(fichaY+1)).css({"opacity":"0.3"});
    }
    break;
    case"reina":
    //MOVIMIENTO ALFIL
    for(i=fichaX-1,j=fichaY+1;i>=0&&j<=7;i--,j++){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
    for(i=fichaX+1,j=fichaY-1;i<=7&&j>=0;i++,j--){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=fichaX+1,j=fichaY+1;i<=7&&j<=7;i++,j++){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=fichaX-1,j=fichaY-1;i>=0&&j>=0;i--,j--){
      if($("#casilla"+(i)+"-"+(j)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(j)).data("color")!=color){
          $("#casilla"+(i)+"-"+(j)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
      //MOVIMIENTO TORRE
      for(i=(fichaX-1);i>=0;i--){
      if($("#casilla"+(i)+"-"+(fichaY)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(fichaY)).data("color")!=color){
          $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
    for(i=(fichaX+1);i<=7;i++){
      if($("#casilla"+(i)+"-"+(fichaY)).data("ocupado")!=true){
        $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(i)+"-"+(fichaY)).data("color")!=color){
          $("#casilla"+(i)+"-"+(fichaY)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=(fichaY+1);i<=7;i++){
      if($("#casilla"+(fichaX)+"-"+(i)).data("ocupado")!=true){
        $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(fichaX)+"-"+(i)).data("color")!=color){
          $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=8;
      }
    }
    for(i=(fichaY-1);i>=0;i--){
      if($("#casilla"+(fichaX)+"-"+(i)).data("ocupado")!=true){
        $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
      }
      else{
        if($("#casilla"+(fichaX)+"-"+(i)).data("color")!=color){
          $("#casilla"+(fichaX)+"-"+(i)).css({"opacity":"0.3"});
        }
        casillaOcupada=true;
        i=-1;
      }
    }
    break;
    }


  }
  //DEVOLVEMOS EL COLOR DE LA CASILLA TRAS FINALIZAR EL MOVIMIENTO
  function casillasBlancas( evs, ui ) {
 
for(i=0;i<=7;i++){
  for(j=0;j<=7;j++){
     $("#casilla"+i+"-"+j).css({"opacity":"1"});
  }

}

}
//PASAMOS DE TURNO AL MOVER FICHA Y CAMBIAMOS DE TURNO Y DE COLOR DE LA PARTIDA
function pasarTurno(){

	if(turno==0){
	nextTurn="negro";
	turno=1;
	}else{
	nextTurn="blanco";
	turno=0;

	}
	cambiarTurno();


	$( "#dialog" ).empty();
	$( "#dialog" ).append( "<p>Turno del jugador "+nextTurn+"</p>" );
	$( "#dialog" ).dialog({height: 200,width:260});
	setTimeout("$( '#dialog' ).dialog('close')",500);
	checkearTurno();



}
//CAMBIAMOS EL TURNO DE COLOR
function cambiarTurno(){
	$.ajax({
	    url: '/fichas/cambiarTurno',
	    dataType: "json",
	    data: {idPartida:idPartida,turno:turno},
	    type: 'POST',
	    
	    success: function(data) {

	     },
	     error: function (xhr, ajaxOptions, thrownError) {
	    console.log(xhr.status);
	    console.log(thrownError);
	  }
	});
}
//CREAMOS TODAS ALS CASILLAS DEL TABLERO CON SUS DATOS INCIALES
function crearTablero(){

    for(i=0;i<8;i++){
      letra="A";
      contador++;

      for(j=0;j<8;j++){
        letraInc=letra.substring(0,letra.length-1)+String.fromCharCode(letra.charCodeAt(letra.length-1)+j);

        contador++;

        $("#tablero").append("<div class='casillaTablero' id='casilla"+i+"-"+j+"''>"+i+"-"+letraInc+"</div>");
        if(contador%2==0){
          if(i==0||i==1||i==6||i==7){
            if(i==0||i==1){
              $("#casilla"+i+"-"+j).css({"background-color":"#51473B","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black"}).data({"posicionX":i,"posicionY":j,"ocupado":true,"color":"negro","ficha":"none"}).droppable({drop: colocar});
            }
            if(i==6||i==7){
              $("#casilla"+i+"-"+j).css({"background-color":"#51473B","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black"}).data({"posicionX":i,"posicionY":j,"ocupado":true,"color":"blanco","ficha":"none"}).droppable({drop: colocar});
            }
            
          }else{
            $("#casilla"+i+"-"+j).css({"background-color":"#51473B","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black"}).data({"posicionX":i,"posicionY":j,"ocupado":false,"color":"none","ficha":"none"}).droppable({drop: colocar});
          }
          
        }else{
          if(i==0||i==1||i==6||i==7){
            if(i==0||i==1){
           $("#casilla"+i+"-"+j).css({"background-color":"#B1ADA8","width":tableroW/8,"height":tableroH/8,"float":"left"}).data({"posicionX":i,"posicionY":j,"ocupado":true,"color":"negro","ficha":"none"}).droppable({drop: colocar});
            }
            if(i==6||i==7){
                         $("#casilla"+i+"-"+j).css({"background-color":"#B1ADA8","width":tableroW/8,"height":tableroH/8,"float":"left"}).data({"posicionX":i,"posicionY":j,"ocupado":true,"color":"blanco","ficha":"none"}).droppable({drop: colocar});
            }

           }else{
            $("#casilla"+i+"-"+j).css({"background-color":"#B1ADA8","width":tableroW/8,"height":tableroH/8,"float":"left"}).data({"posicionX":i,"posicionY":j,"ocupado":false,"color":"none","ficha":"none"}).droppable({drop: colocar});
           }
           

        
        }

       

      }
    }
  }
  //RECIBIMOS LSO WEBSOCKETS
function testWebsock(message){
//SI EL ID DE PARTIDA RECIBIDO ES EL MIMSO QUE NUESTRA PARTIDA EMPEZARAN A EJECUTARSE FUNCIONES DE MOVIMIENTO Y ESCRITURRA DE MENSAJES
if(message.partidaId==idPartida){

casillaOld="#casilla"+message.fichaX+"-"+message.fichaY;
casillaNew="#casilla"+message.huecoX+"-"+message.huecoY;
idFicha=message.nombreFicha;
message.nombreFicha="#"+message.nombreFicha;

casilla="#casilla"+message.huecoX+"-"+message.huecoY;

if(message.jugador != jugador){


//CAMBIAMOS LAS FICAHS DE POSICION
//ELIMINNADO LA ANTIGUA Y CREANDO UNA NUEVA EN LA POSICION MOVIDA
$(message.nombreFicha).data( 'posicionX',parseInt(message.huecoX) );
$(message.nombreFicha).data( 'posicionY',parseInt(message.huecoY) );
$(casillaOld).data("ocupado",false);
$(casillaOld).data("ficha",'none');
$(casillaNew).data("ocupado",true);
$(casillaNew).data("ocupado",true);
$(casillaNew).data("color",message.color);
$(casillaNew).data("ficha",idFicha);

$(casillaOld).data("color",'none');
if(message.huecoFicha!="none"){
  message.huecoFicha="#"+message.huecoFicha;
  $(message.huecoFicha).css('visibility','hidden');
}


//HACEMOS UNA PEQUEÑA ANIMACION PARA VER LA FICHA QUE SE HA CAMBIADO DE POSICION
$(message.nombreFicha).animate("slow").position({my: "center",at: "center",of: casilla});
$(message.nombreFicha).effect( "highlight" );
 pasarTurno();

};
}



}
//ENVIAMOS LOS DATOS DE LAS FICHAS A CAMBIAR DE POSICION A LOS WEBSOCKETS CON TODOS LOS DATOS A USAR POSTERIORMENTE
function testWebsocket(hX,hY,fi,co,nom,jug,fX,fY,hF){
 
$.ajax({
    url: '/webso/test',
    dataType: "json",
    data: {huecoX:hX,huecoY:hY,ficha:fi,color:co,nombreFicha:nom,jugador:jug,fichaX:fX,fichaY,fY,huecoFicha:hF,partidaId:idPartida},
    type: 'POST',
    
    success: function(data) {
    send(data);

//BUSCAR FUNCION PARA RECOLOCAR FICHAS DEL COLOR QUE TOQUE Y PENSAR COMO PASAR VALORES PARA CONTEMPLAR UNA BASE DE DATOS CORRECTA :)

     },
     error: function (xhr, ajaxOptions, thrownError) {
    console.log(xhr.status);
    console.log(thrownError);
  }
});





}
//MATAMOS LA FICHA CAMBIANDO SU ESTADO EN LA BASE DE DATOS A MUERTA
  function matarFicha(ficha){

    $.ajax({
        url: '/fichas/matarFicha',
        dataType: "json",
        async:true,
        data: {idPartida:idPartida,ficha:ficha},
        type: 'POST',
        
        success: function(data) {
        console.log(data);
    

         },
         error: function (xhr, ajaxOptions, thrownError) {
        console.log(xhr.status);
        console.log(thrownError);
      }
    });

 
}


  //EL MOVIMIMENTO CONTROLADO DE TODAS LAS FICHAS EN LA CASILLA ADECUADA POR REGLAS DE MOVIMIENTO
  function colocar(event, evs){
   evs.draggable.draggable( 'option', 'revert', true );
    var huecoX = $(this).data( 'posicionX' );
    huecoY = $(this).data( 'posicionY' );
    fichaX = evs.draggable.data( 'posicionX' );
    fichaY = evs.draggable.data( 'posicionY' );
    ficha=evs.draggable.data( 'ficha' );
    color=evs.draggable.data( 'color' );
    colorH=$(this).data( 'color' );
    ocupado=$(this).data( 'ocupado' );
    huecoFicha=$(this).data( 'ficha' );
    nombreFicha=evs.draggable.attr('id');
    casillaOcupada=false;
    posibleAlfil=false;
    posibleTorre=false;
    torreBorrado=false;
    cambioAlfil=false;

//CREAMOS UN SWITCHCASE PARA CAD TIPO DE FICHA MOVIDA
    switch (ficha) {
    case "peon":
    //COMPORBAMOS EL COLOR SI ESTA OCUPADA LA CASILLA DONDE SE VA A DEJAR LA FICHA O SI LA RUTA LA CORTA UNA FICHA 
                    if (color=="negro"){
                                        
                      
                 
                        if ((huecoX==(fichaX+2)&&huecoY==fichaY)|| huecoX == (fichaX+1)&&(huecoY==fichaY||(huecoY==(fichaY+1))||(huecoY==(fichaY-1)))) {

                        

                          if((ocupado==false&&huecoX == (fichaX+2)&&(huecoY==fichaY)&&evs.draggable.data('primer')==false)||(ocupado==false&&huecoX == (fichaX+1)&&(huecoY==fichaY))||((ocupado==true)&&(color!=colorH)&&((huecoY==(fichaY+1))||(huecoY==(fichaY-1))))){
                            if(huecoX == (fichaX+2)){
                              if($("#casilla"+(fichaX+1)+"-"+huecoY).data("ocupado")==true){
                                casillaOcupada=true;

                              }
                            }
                            if (ocupado==true) {

                              vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              //ENVIAMOS LA FICHA AL CEMENTERIO Y LA ESCONDEMOS
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);

                            };
                           
                            if(casillaOcupada!=true){

                            		//SI E MOVIMIENTO ES INCOMPATIBLE VOLVERA A LA CASILLA DESDE DONDE LA ARRASTRAMOS
                                    evs.draggable.draggable( 'option', 'revert', false );

                                evs.draggable.data( 'primer',true );
                              
          
                     
                     evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                     evs.draggable.data( 'posicionX',huecoX );
                     evs.draggable.data( 'posicionY',huecoY );
                     //TRAS MOVER LA FICHA ENVIAMOS LOS DATOS POR WEBSOCKETS PARA REALIZAR EL MOVIMIENTO
                      testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                      cambiarPosicionFicha(nombreFicha,huecoX,huecoY);

                     $(this).data( 'ocupado',true );
                     $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                     $(this).data( 'color',color );
                      
                     if(huecoX==7){
                      evs.draggable.attr('id',"reinaB5");
                      evs.draggable.removeClass( "peonB" ).addClass( "reinaB" );
                      evs.draggable.data( 'ficha',"reina" );
                      nombreFicha=evs.draggable.attr('id');
                     }
                     $(this).data( 'ficha',nombreFicha );
                     if(finJuego!=true){pasarTurno();}
}
                          }
                                
                            
                          }
                  

                }else{

                  if ((huecoX==(fichaX-2)&&huecoY==fichaY)|| huecoX == (fichaX-1)&&(huecoY==fichaY||(huecoY==(fichaY+1))||(huecoY==(fichaY-1)))) {

                          

                          if((ocupado==false&&huecoX == (fichaX-2)&&(huecoY==fichaY)&&evs.draggable.data('primer')==false)||(ocupado==false&&huecoX == (fichaX-1)&&(huecoY==fichaY))||((ocupado==true)&&(color!=colorH)&&((huecoY==(fichaY+1))||(huecoY==(fichaY-1))))){
                            if(huecoX == (fichaX-2)){
                              if($("#casilla"+(fichaX-1)+"-"+huecoY).data("ocupado")==true){
                                casillaOcupada=true;

                              }
                            }
                            if (ocupado==true) {
                              vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);

                            };
                              if(casillaOcupada!=true){

                                    evs.draggable.draggable( 'option', 'revert', false );
                                    evs.draggable.data( 'primer',true );

                                    testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                                    cambiarPosicionFicha(nombreFicha,huecoX,huecoY);


                     
                     evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                     evs.draggable.data( 'posicionX',huecoX );
                     evs.draggable.data( 'posicionY',huecoY );

                     $(this).data( 'ocupado',true );
                     $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                     $(this).data( 'color',color );
                     if(huecoX==0){
                      evs.draggable.attr('id',"reinaB5");
                      evs.draggable.removeClass( "peonW" ).addClass( "reinaW" );
                      evs.draggable.data( 'ficha',"reina" );
                      nombreFicha=evs.draggable.attr('id');
                      
                     }
                     $(this).data( 'ficha',nombreFicha );
                     if(finJuego!=true){pasarTurno();}

                          }
                        }
                                
                            
                          }
                                  
                  }
                       

                   
                  
                        
        break;
    case "torre":
    //REPETIMOS LOS MOVIMIENTOS ANTERIORES CON TODO TIPO DE FICHAS
        //COMPORBAMOS EL COLOR SI ESTA OCUPADA LA CASILLA DONDE SE VA A DEJAR LA FICHA O SI LA RUTA LA CORTA UNA FICHA 
     if ( huecoX == fichaX||huecoY==fichaY) {




                          if (huecoY == fichaY) {

                        


                        if(fichaX<huecoX){

                           for(i=fichaX+1;i<=huecoX;i++){
                         
                          if ($("#casilla"+i+"-"+fichaY).data("ocupado")==true&&i<huecoX) {
                            casillaOcupada=true;
                            
                          };
                          
                          if(casillaOcupada!=true&&i==huecoX&&color!=colorH&&ocupado==true){
                            //alert("torremato");
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);


                          }
                          if(casillaOcupada!=true&&i==huecoX&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }


                         }
                          
                        }
                        if(fichaX>huecoX){
                          
                          for(i=fichaX-1;i>=huecoX;i--){
                   
                            if($("#casilla"+i+"-"+fichaY).data("ocupado")==true&&i==huecoX&&casillaOcupada!=true&&color!=colorH){
                            
                              vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                              torreBorrado=true;
                              casillaOcupada=false;
                            }
                            if ($("#casilla"+i+"-"+fichaY).data("ocupado")==true&&i>=huecoX&&torreBorrado==false) {
                            casillaOcupada=true;
                            
                          };
                            
                         
                        
                        
                          

                         }
                          
                        }

                        
}

                          if (huecoX == fichaX) {
             
                        if(fichaY<huecoY){
                   
                           for(i=fichaY+1;i<=huecoY;i++){
                         if ($("#casilla"+fichaX+"-"+i).data("ocupado")==true&&i<huecoY) {
                  casillaOcupada=true;
                };
                if(casillaOcupada!=true&&i==huecoY&&color!=colorH&&ocupado==true){
                        
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                            
                }
                if(casillaOcupada!=true&&i==huecoY&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }
                         }
                        }
                        if(fichaY>huecoY){
                          
                          for(i=fichaY-1;i>=huecoY;i--){
   
                            if ($("#casilla"+fichaX+"-"+i).data("ocupado")==true&&i>huecoY) {
                            casillaOcupada=true;
                };
                if(casillaOcupada!=true&&i==huecoY&&color!=colorH&&ocupado==true){
                            
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);

                            
                }
                if(casillaOcupada!=true&&i==huecoY&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }                        
                        
                         }                        
                        }

                        
}

                         if (casillaOcupada!=true&&((huecoY!=fichaY||huecoX!=fichaX))){
                         


                                    evs.draggable.draggable( 'option', 'revert', false );
               
                     testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                     cambiarPosicionFicha(nombreFicha,huecoX,huecoY);
                     evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                     evs.draggable.data( 'posicionX',huecoX );
                     evs.draggable.data( 'posicionY',huecoY );

                     $(this).data( 'ocupado',true );
                     $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                     $(this).data( 'color',color );

                     $(this).data( 'ficha',nombreFicha );
                     if(finJuego!=true){pasarTurno();}

                        
                               } 
                            
                          }
        
        break;
    case "caballo":
        //REPETIMOS LOS MOVIMIENTOS ANTERIORES CON TODO TIPO DE FICHAS
        //COMPORBAMOS EL COLOR SI ESTA OCUPADA LA CASILLA DONDE SE VA A DEJAR LA FICHA O SI LA RUTA LA CORTA UNA FICHA 
                         if ( (huecoX == fichaX+2&&huecoY==fichaY+1)||(huecoX == fichaX+1&&huecoY==fichaY+2)||(huecoX == fichaX-1&&huecoY==fichaY+2)||(huecoX == fichaX-2&&huecoY==fichaY+1)||(huecoX == fichaX-1&&huecoY==fichaY-2)||(huecoX == fichaX-2&&huecoY==fichaY-1)||(huecoX == fichaX+2&&huecoY==fichaY-1)||(huecoX == fichaX+1&&huecoY==fichaY-2)) {
                 
                          if((color!=colorH&&ocupado==true)||(color==colorH&&ocupado==false)||(color!=colorH&&ocupado==false)){
                            
                            if((color!=colorH&&ocupado==true)){
                             vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                            }
                            

                          


                              evs.draggable.draggable( 'option', 'revert', false );
                                          
                                             testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                                             cambiarPosicionFicha(nombreFicha,huecoX,huecoY);
                                             evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                                             evs.draggable.data( 'posicionX',huecoX );
                                             evs.draggable.data( 'posicionY',huecoY );
                                      

                                             $(this).data( 'ocupado',true );
                                             $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                                             $(this).data( 'color',color );
                                             $(this).data( 'ficha',nombreFicha );
                                             if(finJuego!=true){pasarTurno();}
                                            
                                           }
                                           }
        
        break;
    case "alfil":
       //REPETIMOS LOS MOVIMIENTOS ANTERIORES CON TODO TIPO DE FICHAS
        //COMPORBAMOS EL COLOR SI ESTA OCUPADA LA CASILLA DONDE SE VA A DEJAR LA FICHA O SI LA RUTA LA CORTA UNA FICHA 
    rutaAlfil=huecoX+"-"+huecoY;
     
                    for(i=fichaX+1,j=fichaY+1;i<=7&&j<=7;i++,j++){
               
                      if (posibleAlfil==true){
                        if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i<=huecoX&&j<=huecoY)) {
                     

                          if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }

                            
                            
                          };
                          if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                        
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);


                          }

                      }
                      if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                        

                        posibleAlfil=true;
                        i=(fichaX);
                        j=(fichaY);

                      }


                    }
                    if (posibleAlfil!=true){
                    for(i=fichaX+1,j=fichaY-1;i<=7&&j>=0;i++,j--){
                     
                      if (posibleAlfil==true){
                        if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i<=huecoX&&j>=huecoY)) {
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
                            
                          };
                          if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                            
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);


                          }

                      }
                      if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                       
                        posibleAlfil=true;
                        i=fichaX;
                        j=fichaY;
                      }


                    }
                  }
                  if (posibleAlfil!=true){

                     for(i=fichaX-1,j=fichaY-1;i>=0&&j>=0;i--,j--){
                    
                      if (posibleAlfil==true){
                        if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i>=huecoX&&j>=huecoY)) {
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
                            
                          };
                          if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                           
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);


                          }

                      }
                     if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                      
                        posibleAlfil=true;
                        i=fichaX;
                        j=fichaY;
                      }



                    }
                  }
                  if (posibleAlfil!=true){
                    for(i=fichaX-1,j=fichaY+1;i>=0&&j<=7;i--,j++){
                     
                      if (posibleAlfil==true){
                        if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i>=huecoX&&j<=huecoY)) {
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
                            
                          };
                          if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                          
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);


                          }

                      }
                      if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                        
                        posibleAlfil=true;
                        i=fichaX;
                        j=fichaY;
                      }


                    }
                  }
if(casillaOcupada!=true){
  if(posibleAlfil==true){

  evs.draggable.draggable( 'option', 'revert', false );
                                          
                                             testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                                             cambiarPosicionFicha(nombreFicha,huecoX,huecoY);
                                             evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                                             evs.draggable.data( 'posicionX',huecoX );
                                             evs.draggable.data( 'posicionY',huecoY );

                                             $(this).data( 'ocupado',true );
                                             $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                                             $(this).data( 'color',color );
                                             $(this).data( 'ficha',nombreFicha );
                                             if(finJuego!=true){pasarTurno();}
                                           }
}




        
        break;
    case "rey":
        //REPETIMOS LOS MOVIMIENTOS ANTERIORES CON TODO TIPO DE FICHAS
        //COMPORBAMOS EL COLOR SI ESTA OCUPADA LA CASILLA DONDE SE VA A DEJAR LA FICHA O SI LA RUTA LA CORTA UNA FICHA 

                  if ((huecoX==fichaX-1&&huecoY==fichaY-1)||(huecoX==fichaX-1&&huecoY==fichaY)||(huecoX==fichaX-1&&huecoY==fichaY+1)||(huecoX==fichaX&&huecoY==fichaY+1)||(huecoX==fichaX+1&&huecoY==fichaY+1)||(huecoX==fichaX+1&&huecoY==fichaY)||(huecoX==fichaX+1&&huecoY==fichaY-1)||(huecoX==fichaX&&huecoY==fichaY-1)) {
                
                    if (ocupado==true){
 if(color!=colorH&&ocupado==true){
                           
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);

                                           }else{
                                            casillaOcupada=true;
                                           }

                          }
                          if(casillaOcupada!=true){
                          evs.draggable.draggable( 'option', 'revert', false );
                                            
                                             testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                                             cambiarPosicionFicha(nombreFicha,huecoX,huecoY);
                                             evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                                             evs.draggable.data( 'posicionX',huecoX );
                                             evs.draggable.data( 'posicionY',huecoY );

                                             $(this).data( 'ocupado',true );
                                             $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                                             $(this).data( 'color',color );
                                             $(this).data( 'ficha',nombreFicha );
                                             if(finJuego!=true){pasarTurno();}
                                           }
                    }
                   

                    
        
        break;
    case "reina":
//LA REINA COMBINA MOVIMIENTOS DE TORRE ALFIL
    //REPETIMOS LOS MOVIMIENTOS ANTERIORES CON TODO TIPO DE FICHAS
        //COMPORBAMOS EL COLOR SI ESTA OCUPADA LA CASILLA DONDE SE VA A DEJAR LA FICHA O SI LA RUTA LA CORTA UNA FICHA 
        if ( huecoX == fichaX||huecoY==fichaY) {

          if (huecoY == fichaY) {
            
            if(fichaX<huecoX){
              for(i=fichaX+1;i<=huecoX;i++){
               
                if ($("#casilla"+i+"-"+fichaY).data("ocupado")==true&&i<huecoX) {
                
                  casillaOcupada=true;
                };
                if(casillaOcupada!=true&&i==huecoX&&color!=colorH&&ocupado==true){
                 
                  casillaOcupada=false;
                  vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                  
                }
                if(casillaOcupada!=true&&i==huecoX&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }
              }
            }      
            if(fichaX>huecoX){
              
              for(i=fichaX-1;i>=huecoX;i--){
            
                if ($("#casilla"+i+"-"+fichaY).data("ocupado")==true&&i>huecoX) {
                 
                  casillaOcupada=true;
                };
             
                        
                if(casillaOcupada!=true&&i==huecoX&&color!=colorH&&ocupado==true){
                  
                  casillaOcupada=false;
                  vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                  
                }
                if(casillaOcupada!=true&&i==huecoX&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }

              }
            }
          }
          if (huecoX == fichaX) {
  
            if(fichaY<huecoY){
              for(i=fichaY+1;i<=huecoY;i++){
                if ($("#casilla"+fichaX+"-"+i).data("ocupado")==true&&i<huecoY) {
                  casillaOcupada=true;
                };
                if(casillaOcupada!=true&&i==huecoY&&color!=colorH&&ocupado==true){
                           
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                            
                }
                if(casillaOcupada!=true&&i==huecoY&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }
              }
            }
            if(fichaY>huecoY){
              
              for(i=fichaY-1;i>=huecoY;i--){
                if ($("#casilla"+fichaX+"-"+i).data("ocupado")==true&&i>huecoY) {
                            casillaOcupada=true;
                };
                if(casillaOcupada!=true&&i==huecoY&&color!=colorH&&ocupado==true){
                           
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                            
                }
                if(casillaOcupada!=true&&i==huecoY&&color==colorH&&ocupado==true){
                  casillaOcupada=true;
                }
              }
            }
          }
     
          if(casillaOcupada==false){posibleTorre=true;}
        }else{
          
          rutaAlfil=huecoX+"-"+huecoY;
            
          for(i=fichaX+1,j=fichaY+1;i<=7&&j<=7;i++,j++){
                     
            if (posibleAlfil==true){
              if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i<=huecoX&&j<=huecoY)) {
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
              };
              if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                            
                            casillaOcupada=false;
                           vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
              }
            }
            if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                       

                        posibleAlfil=true;
                        i=(fichaX);
                        j=(fichaY);
            }
          }
          if (posibleAlfil!=true){
            for(i=fichaX+1,j=fichaY-1;i<=7&&j>=0;i++,j--){
                     
              if (posibleAlfil==true){
                if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i<=huecoX&&j>=huecoY)) {
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
                };
                if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                         
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                }
              }
              if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                       
                        posibleAlfil=true;
                        i=fichaX;
                        j=fichaY;
              }
            }
          }
          if (posibleAlfil!=true){
            for(i=fichaX-1,j=fichaY-1;i>=0&&j>=0;i--,j--){
                      
              if (posibleAlfil==true){


                if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i>=huecoX&&j>=huecoY)) {
            
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
                };
                if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                           
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                }
              }
              if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                       
                        posibleAlfil=true;
                        i=fichaX;
                        j=fichaY;
              }
            }
          }
          if (posibleAlfil!=true){
            for(i=fichaX-1,j=fichaY+1;i>=0&&j<=7;i--,j++){
                     
              if (posibleAlfil==true){
                if ($("#casilla"+i+"-"+j).data("ocupado")==true&&(i>=huecoX&&j<=huecoY)) {
                            if(huecoY==j&&huecoX==i&&casillaOcupada==false&&colorH!=color){


                          }else{
                            casillaOcupada=true;
                          }
                };
                if(casillaOcupada!=true&&i==huecoX&&j==huecoY&&color!=colorH&&ocupado==true){
                            
                            casillaOcupada=false;
                            vFicha=$("#"+huecoFicha).data("ficha");
                              $( "#"+huecoFicha ).css('visibility','hidden');
                              
                              alCementerio(vFicha,colorH,huecoFicha);
                              matarFicha(huecoFicha);
                }
              }
              if(rutaAlfil==(i+"-"+j)&&posibleAlfil!=true){
                      
                        posibleAlfil=true;
                        i=fichaX;
                        j=fichaY;
              }
            }
          }
        }
          if(casillaOcupada!=true){
            if((posibleAlfil==true||posibleTorre==true)&&(huecoY!=fichaY||huecoX!=fichaX)){


              evs.draggable.draggable( 'option', 'revert', false );
                                            
                                             testWebsocket(huecoX,huecoY,ficha,color,nombreFicha,jugador,fichaX,fichaY,huecoFicha);
                                             cambiarPosicionFicha(nombreFicha,huecoX,huecoY);
                                             evs.draggable.position( { of: $(this), my: 'center', at: 'center' });
                                             evs.draggable.data( 'posicionX',huecoX );
                                             evs.draggable.data( 'posicionY',huecoY );

                                             $(this).data( 'ocupado',true );
                                             $("#casilla"+fichaX+"-"+fichaY).data("ocupado",false);
                                             $(this).data( 'color',color );
                                             $(this).data( 'ficha',nombreFicha );
                                             if(finJuego!=true){pasarTurno();}
            }
          }



        
        break;
} 

}
//CREAMOS TODAS LAS FICHAS LLAMANDO A SUS FUNCIONES DE CREACION
  function crearFichas(){
crearPeones();
crearTorres();
crearCaballos();
crearAlfiles();
crearReyes();
crearReinas();

  }
  function movimientos(){
  
  }
  function movimientosPeones(casilla){

  }
  //CREAMOS LOS PEONES CONS USS VALORES INICIALES
  function crearPeones(){
    


    for(i=0;i<8;i++){

    $("#ajedrez").append("<div class='peonB'id='peonN"+i+"'></div>");
    $("#peonN"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {cursor: 'move',containment: '#tablero',revert: true,drag:cerrarMano,start: casillasRojas,stop: casillasBlancas,disabled:true}).data({"posicionX":1,"posicionY":i,"ficha":"peon","color":"negro","primer":false});
    $( "#peonN"+i ).position({my: "center",at: "center",of: "#casilla1-"+i});
    $("#ajedrez").append("<div class='peonW'id='peonB"+i+"'></div>");
    $("#peonB"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {drag:cerrarMano,containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas}).data({"posicionX":6,"posicionY":i,"ficha":"peon","color":"blanco","primer":false});
    $( "#peonB"+i ).position({my: "center",at: "center",of: "#casilla6-"+i});
    $("#casilla6-"+i).data("ficha","peonB"+i);
    $("#casilla1-"+i).data("ficha","peonN"+i);

}

  }
  //CREAMOS LAS TORRES CONS USS VALORES INICIALES
  function crearTorres(){

    for(i=0;i<8;i++){

    $("#ajedrez").append("<div class='torreB' id='torreN"+i+"'></div>");
    $("#torreN"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas,disabled:true}).data({"posicionX":0,"posicionY":i,"ficha":"torre","color":"negro"});
    $( "#torreN"+i ).position({my: "center",at: "center",of: "#casilla0-"+i});
    $("#ajedrez").append("<div class='torreW' id='torreB"+i+"'></div>");
    $("#torreB"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas}).data({"posicionX":7,"posicionY":i,"ficha":"torre","color":"blanco"});
    $( "#torreB"+i ).position({my: "center",at: "center",of: "#casilla7-"+i});
    $("#casilla0-"+i).data("ficha","torreN"+i);
    $("#casilla7-"+i).data("ficha","torreB"+i);
    i=i+6;

}
  }
  //CREAMOS LOS CABALLOS CONS USS VALORES INICIALES
   function crearCaballos(){

    for(i=1;i<7;i++){

    $("#ajedrez").append("<div class='caballoB' id='caballoN"+i+"'></div>");
    $("#caballoN"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas,disabled:true}).data({"posicionX":0,"posicionY":i,"ficha":"caballo","color":"negro"});
    $( "#caballoN"+i ).position({my: "center",at: "center",of: "#casilla0-"+i});
    $("#ajedrez").append("<div class='caballoW' id='caballoB"+i+"'></div>");
    $("#caballoB"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas}).data({"posicionX":7,"posicionY":i,"ficha":"caballo","color":"blanco"});
    $( "#caballoB"+i ).position({my: "center",at: "center",of: "#casilla7-"+i});
    $("#casilla0-"+i).data("ficha","caballoN"+i);
    $("#casilla7-"+i).data("ficha","caballoB"+i);
    i=i+4;

}
  }
  //CREAMOS LOS ALFILES CONS USS VALORES INICIALES
  function crearAlfiles(){


    for(i=2;i<7;i++){

    $("#ajedrez").append("<div class='alfilB' id='alfilN"+i+"'></div>");
    $("#alfilN"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas,disabled:true}).data({"posicionX":0,"posicionY":i,"ficha":"alfil","color":"negro"});
    $( "#alfilN"+i ).position({my: "center",at: "center",of: "#casilla0-"+i});
    $("#ajedrez").append("<div class='alfilW' id='alfilB"+i+"'></div>");
    $("#alfilB"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas}).data({"posicionX":7,"posicionY":i,"ficha":"alfil","color":"blanco"});
    $( "#alfilB"+i ).position({my: "center",at: "center",of: "#casilla7-"+i});
    $("#casilla0-"+i).data("ficha","alfilN"+i);
    $("#casilla7-"+i).data("ficha","alfilB"+i);
    i=i+2;

}
  }
  //CREAMOS LOS RESYES CONS USS VALORES INICIALES
    function crearReyes(){

    for(i=3;i<4;i++){

    $("#ajedrez").append("<div class='reyB' id='reyN"+i+"'></div>");
    $("#reyN"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas,disabled:true}).data({"posicionX":0,"posicionY":i,"ficha":"rey","color":"negro"});
    $( "#reyN"+i ).position({my: "center",at: "center",of: "#casilla0-"+i});
    $("#ajedrez").append("<div class='reyW' id='reyB"+i+"'></div>");
    $("#reyB"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas}).data({"posicionX":7,"posicionY":i,"ficha":"rey","color":"blanco"});
    $( "#reyB"+i ).position({my: "center",at: "center",of: "#casilla7-"+i});
    $("#casilla0-"+i).data("ficha","reyN"+i);
    $("#casilla7-"+i).data("ficha","reyB"+i);
    i=i+2;

}
  }  
//CREAMOS LAS REINAS CONS USS VALORES INICIALES
  function crearReinas(){

    for(i=4;i<5;i++){

    $("#ajedrez").append("<div class='reinaB' id='reinaN"+i+"'></div>");
    $("#reinaN"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas,disabled:true}).data({"posicionX":0,"posicionY":i,"ficha":"reina","color":"negro"});
    $( "#reinaN"+i ).position({my: "center",at: "center",of: "#casilla0-"+i});
    $("#ajedrez").append("<div class='reinaW' id='reinaB"+i+"'></div>");
    $("#reinaB"+i).css({"background-color":"none","width":tableroW/8,"height":tableroH/8,"float":"left","color":"black","opacity":"1","position":"relative"}).draggable( {containment: '#tablero',revert: true,start: casillasRojas,stop: casillasBlancas}).data({"posicionX":7,"posicionY":i,"ficha":"reina","color":"blanco"});
    $( "#reinaB"+i ).position({my: "center",at: "center",of: "#casilla7-"+i});
    $("#casilla0-"+i).data("ficha","reinaN"+i);
    $("#casilla7-"+i).data("ficha","reinaB"+i);
    i=i+2;

}


  }