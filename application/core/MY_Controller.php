<?php
class Auth_Controller extends CI_Controller {

    function __construct()
    {

        parent::__construct();
            	$this->load->library('session');
            	 $this->load->helper('url');
        if ( ! $this->session->userdata('logged_in'))
        { 
            redirect('login');
        }else{
        	//redirect('nologin');
        }
    }
}