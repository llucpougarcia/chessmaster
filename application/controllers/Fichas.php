<?php
class Fichas extends CI_Controller {

        public function view($page = 'contacto')
{
        $this->load->model('fichas_model');
    
       if ( ! file_exists(APPPATH.'views/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
                
        

        $this->load->helper('url');
        $this->load->view('templates/simplyheader');
        //$this->load->view('test'); 
        //$this->load->view('pages/'.$page, $data);
        $this->load->view('contacto');
        $this->load->view('templates/footer');


        
}
public function cogerTurno(){
        $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');


         $data3 = $this->fichas_model->coger_turno($idPartida);
         echo json_encode($data3);

         //return $data3;
}
public function crearPartida(){
        $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');
         $colorJugador= $this->input->post('Color');


         $data3 = $this->fichas_model->crear_partida($idPartida,$colorJugador);
         echo json_encode($data3);

}
public function cambiarTurno(){
        $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');
         $turno = $this->input->post('turno');


         $data3 = $this->fichas_model->cambiar_turno($idPartida,$turno);
         echo json_encode($data3);

         //return $data3;
}
public function checkColor(){

    $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');
         


         $data3 = $this->fichas_model->check_color($idPartida);
         echo json_encode($data3);
 

    }
    public function takeGuest(){

    $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');
         


         $data3 = $this->fichas_model->take_guest($idPartida);
         echo json_encode($data3);
        

    }
public function cogerFichas(){
        $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');


         $data3 = $this->fichas_model->coger_fichas($idPartida);
         echo json_encode($data3);

         //return $data3;
}

public function cambiarPosicion(){

        $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');
         $ejeX = $this->input->post('ejeX');
         $ejeY = $this->input->post('ejeY');
         $nombreFicha = $this->input->post('nombreFicha');
         if(stripos($nombreFicha,'peon') !== false){
                $primer=1;
         }else{
                $primer=0;
         }

         $data3 = $this->fichas_model->cambiar_posicion($idPartida,$nombreFicha,$ejeX,$ejeY,$primer);
         echo json_encode($data3);

}
public function matarFicha(){

           $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');
         $ficha = $this->input->post('ficha');


         $data3 = $this->fichas_model->matar_ficha($idPartida,$ficha);
         echo json_encode($data3);

}
public function crearFichas(){
        $this->load->model('fichas_model');

         $idPartida = $this->input->post('idPartida');


         $data3 = $this->fichas_model->crear_fichas($idPartida);
         echo json_encode($data3);

         //return $data3;
}

}