<?php
class Main extends Auth_Controller {

        public function view($page = 'home')
{
       // $this->session->sess_destroy();
        $this->load->model('main_model');
        if ( ! file_exists(APPPATH.'views/pages/'.$page.'.php'))
        {
                // Whoops, we don't have a page for that!
                show_404();
        }
        $idPartida = $this->input->post('idPartida');
         $color = $this->input->post('color');
        $data2['tot'] = $this->main_model->test($color,$idPartida);
        
                
        $data['title'] = ucfirst($page); // Capitalize the first letter
        
      

        $this->load->helper('url');
        $this->load->view('templates/header', $data);
        //$this->load->view('test'); 
        //$this->load->view('pages/'.$page, $data);
        $this->load->view('news/index', $data2);
        $this->load->view('templates/footer', $data);

        
}
}

