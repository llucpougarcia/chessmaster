
<html>
  <head>
    <title>LPGCFGSDAWPF</title>

 
      <link rel="stylesheet" href="../../assets/css/font-awesome-4.7.0/css/font-awesome.min.css">


 <link rel="shortcut icon" type="image/x-icon" href="../../assets/css/images/favicon.ico">

      <link rel="stylesheet" type="text/css" href="../../assets/css/jquery-ui.css" />  
      <link rel="stylesheet" type="text/css" href="../../assets/css/jquery-ui.structure.css" />  
      <link rel="stylesheet" type="text/css" href="../../assets/css/jquery-ui.theme.css" />  
      <link rel="stylesheet" type="text/css" href="../../assets/css/style.css" />  
      <script type="text/javascript" src="../../assets/js/jquery.js"></script>
      <script type="text/javascript" src="../../assets/js/jquery-ui.js"></script>
      <script type="text/javascript" src="../../assets/js/scripts.js"></script>
            <link rel="stylesheet" href="../../assets/bootstrap-3.3.7-dist/css/bootstrap.min.css"  crossorigin="anonymous">
      <script src="../../assets/bootstrap-3.3.7-dist/js/bootstrap.min.js"  crossorigin="anonymous"></script>
      <script src="assets/js/fancywebsocket.js"></script>

  </head>
<div id="allContent">
  <div id="topBar">
  <a href="/"><img src="../../assets/css/images/chessLogo.png"></a>
<div class="logedContainer">
  <div class="iconName">

    <div class="logedName"><?php 
  echo($user);?></div>
  <div class="logedIcon"><img src="../../assets/css/images/avatar.png"></div>
  </div>
</div>
  
  </div>
  <div id="sideBar">
  <ul id="menuItems">
    <li class="menuItem" onclick="mostrarNumero();">Nueva Partida</li>
    <li class="menuItem" onclick="introducirNumero();">Cargar Partida</li>
    <a href="/partidas" ><li class="menuItem">Partidas Activas</li></a>
    <li class="menuItem">Perfil</li>
    <li class="menuItem">Ajustes</li>
    <li class="menuItem">Puntuacion</li>
    <a href="/login/logout"><li class="menuItem">Cerrar Sesion</li></a>
  </ul>
  </div>
  <div id="contentWrapper">
  </div>
</div>
</html>